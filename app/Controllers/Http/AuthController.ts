import User from 'App/Models/User'
import Hash from '@ioc:Adonis/Core/Hash'
import { schema } from '@ioc:Adonis/Core/Validator'

export default class AuthController {
  //Autenticación usando API Token
  public async login({ auth, request, response }) {
    const user = request.input('user')
    const key = request.input('key')

    try {
      //Creamos schema para validar datos 
      const validateCreate = schema.create({
        user: schema.string({
          trim: true,
        }),
        key: schema.string({
          trim: true,
        }),
      })
      //mensaje personalizados de validación
      const messages = {
        'user.required': 'user requerido.',
        'key.required': 'key requerido.',
      }

      //validamos los datos NOTA: usar reporter: validator.reporters.vanilla para pintar diferentes las validaciones
      await request.validate({ schema: validateCreate, messages: messages })

      const us = await User.query().where('user', user).firstOrFail()
      console.log(key)
      if (!(await Hash.verify(us.key, key))) {
        return response.badRequest({
          status: false,
          msg: 'Credenciales incorrectas',
          data: null,
        })
      }

      //El token generado siempre se establece en 'bearer'.
      const token = await auth.use('api').generate(us, {
        expiresIn: '480mins',
      })

      return {
        status: true,
        msg: 'Autenticación correcta',
        data: token,
      }
    } catch (error) {
      return response.badRequest({
        status: false,
        msg: 'Error | ' + error.message,
        data: error.messages,
      })
    }
  }

  //Revocar token o cerrar sesión
  public async logout({ auth, response }) {
    try {
      await auth.use('api').revoke()
      return {
        status: true,
        msg: 'Token revocado',
        data: null,
      }
    } catch (error) {
      return response.badRequest({
        status: false,
        msg: 'API token invalido | ' + error.message,
        data: null,
      })
    }
  }
}
